# Nachbericht verfassen

## Bilder

Bitte legt die Bilder unter `public/konzerte` in die jeweiligen Ordner ab. Die Bilder sollten eine Breite von 800px haben.

Folgendes Ordner-Konzept existiert

```
public/konzerte/2023/serenade
  |       |       |     |
  |       |       |     +-- Name der Veranstaltung
  |       |       +-- Jahr der Veranstaltung
  |       +-- Ordner für Konzert-Assets
  +-- Ordner für alle Bilder / PDFs / etc.
```

## Text

Die Nachberichte kommen immer direkt in die Veranstaltung. Die Veranstlatungen sind unter `src/content/konzerte` zu finden. Das Schema für die Datei-Strukturieren ist hier anders herum als bei den Assets. Diese sind hier nach Veranstaltungsart und dann nach Jahr sortiert. Z.B. `serenade/2023.md`.

Die Veranstaltungen werden als [Markdown](https://www.markdownguide.org/cheat-sheet/) - https://www.markdownguide.org/cheat-sheet/ gepflegt.

Das Schema für die Veranstaltungen ist wie folgt:

```md
---
META-INFOS
---

<HIER TEXT EINFÜGEN GEMÄSS MD-Standard>

<!-- So werden Bilder (OHNE 'public') eingefügt -->
![Bild](/konzerte/2023/serenade/20230423-1603-DSC07972.jpg)

```
