# MV Obermeitingen

![Version](https://gitlab.com/forestdesign/mvobermeitingen/website/-/badges/release.svg)

Hier wird beschrieben, wie die einzelnen Seiten gebaut und gepflegt werden.

## Ressourcen / Requirements

| Name                          | Version      |
|-------------------------------|--------------|
| Node.js                       | min. 18.16.0 |
| pnpm                          | min. 6.14.0  |
| Tailwind CSS                  | min. 3.0.0   |
| [Astro](https://astro.build/) | min. 2.9.0   |

## Table of Content

- [Änderungen](CHANGES.md)
- [Neue Konzerte anlegen](KONZERTE.md)
- [Neue Nachberichte schreiben](NACHBERICHT.md)
- [Neue Blogbeiträge schreiben](BLOG.md)
* [Musiker & Vorstandschaft](MUSIKER_VORSTAND.md)
