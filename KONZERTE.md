# Konzert erstellen

## Datenschema

### Konzert-Gruppen

**JSON:**
```json
{
"name": <NAME VON GRUPPE>,     <!-- (Freitext) -->
"order": <REIHENFOLGE>,        <!-- (Ganzzahl; 0 = Erste Gruppe) -->
"stamp": <STAMP>              <!-- (Relativer Pfad zum Stempel-Bild der Kategorie) -->
}
```

**Bestehende Gruppen:**

- Fasching - Angeben als: `fasching`
- In Konzert - Angeben als: `in_konzert`
- Kirchenkonzert - Angeben als: `kirchenkonzert`
- Osterfest - Angeben als: `osterfest`
- Vatertag - Angeben als: `vatertag`
- Serenade - Angeben als: `serenade`
- Vereinsintern - Angeben als: `vereinsintern`

### Konzert

**Markdown:**
```md
---
<!-- In den Strichen kommen die Meta-Informationen Zur Auswahl stehen: -->

title: In Konzert 2022                          <!-- Titel der Veranstaltung -->
description:                                    <!-- Beschreibung der Veranstaltung in zwei kurzen Sätzen -->     
heroImage: konzerte/2022/in_konzert_2022.jpg    <!-- Dateinamen des Plakats -->
date: 2022-11-19T19:30:00+01:00                 <!-- Datum und Uhrzeit. Winter- (+01:00) und Sommerzeit (+02:00) beachten -->
endDate: 2022-11-19T21:00:00+01:00              <!-- Datum und Uhrzeit -->
gruppe_ref: in_konzert                          <!-- Gruppenname, sie unter `src/content/konzert_gruppe/` -->
location: 'Bürgerhaus Obermeitingen'            <!-- Location-Name -->
price: 7.00                                     <!-- Preis als DB-Float (null bei Kostenfrei; 7.00 oder anderer Wert für den Preis) -->
veranstalter: Musikverein Obermeitingen e.V.    <!-- Name des Veranstalters -->

<!-- Optionale Meta-Informationen -->

musiker_ref: [musiker, musiker2]                <!-- Dateinamen der Musiker-JSONs -->
related_blog_ref: 2023/name-des-blogposts       <!-- Dateinamen des Blogposts ohne Ändung aus `src/content/blog/` -->
goody: [Getränke, Bier, Sekt,...]               <!-- Liste der Angebotenen Goodys der Veranstaltung. Gerne viele Punkte als Anreize fürs Kommen -->
sponsoren: [ 2023/sponsor_blackbox.svg, ...]    <!-- Liste der Sponsoren-Logos, diese werden Jährlich gewechselt  -->
locationUrl: https://www.google.de/maps/...     <!-- Link zur Location; Generaueres wird unten beschrieben -->
locationImage: konzerte/2022/bild_location.jpg  <!-- Bild der Location -->
priceDescription:                               <!-- Beschreibung des Preises; z.B. "Eintritt wird gespendet" -->
programm: [<KOMPLEXES PROGRAMM>]                <!-- Programm der Veranstaltung -->
verschoben: false                               <!-- Gibt an, ob die Veranstaltung verschoben wurde -->
abgesagt: false                                 <!-- Gibt an, ob die Veranstaltung abgesagt wurde -->
---
<SPÄTERER NACHBERICHT>
```
### Komplexes Programm
Das Programm ist ein komplexerer Aufbau. Daher eine separate Beschreibung.
```md
---
...
programm: [
  { 
    title: 'Faschingsumzug',                    <!-- Titel des Programmpunkts -->   
    description: 'Treffpunkt: 13:15 Uhr am...'  <!-- Beschreibung des Programmpunkts --> 
    time: 2023-02-12T13:30:00+02:00,            <!-- Uhrzeit des Programmpunkts. Winter- (+01:00) und Sommerzeit (+02:00) beachten -->
    endTime: 2023-02-12T14:00:00+02:00,         <!-- End-Uhrzeit des Programmpunkts. Winter- (+01:00) und Sommerzeit (+02:00) beachten -->
    ca: false                                   <!-- Optional: Gibt an, ob die Uhrzeit nur ca. ist -->
  },
  ...
]
...
---
```

## Konzert anlegen

Um ein neues Konzert anzulegen, müsst ihr als aller Erstes das Plaket erstellen und in den Ordner `public/konzerte` ablegen. Das Plakat kann Originalgröße beibehalten werden. Es wird automatisch skaliert.

Die Konzerte werden dann als Markdown-Datei unter `src/content/konzerte` angelegt. Das Schema für die Datei-Strukturieren ist hier anders herum als bei den Assets. Diese sind hier nach Veranstaltungsart und dann nach Jahr sortiert. Z.B. `serenade/2023.md`.
Sollte es weitere Veranstaltungen wie z.B. zwei Konzerte an unterschiedlichen Tagen geben, so könnt Ihr den Dateinamen einfach mit Worten und Unterstrichen erweitern: `serenade/2023.md` und `serenade/2023_jugend.md`.

Anschließend könnt ihr die Datei mit den Meta-Informationen füllen. Die Meta-Informationen sind oben beschrieben. Die Datei kann dann mit dem Inhalt gefüllt werden. Dieser wird dann auf der Webseite angezeigt.


### Bilder

Folgendes Ordner-Konzept existiert, in der die Bilder abgelegt werden

```
public/konzerte/2023/serenade
  |       |       |     |
  |       |       |     +-- Name der Veranstaltung
  |       |       +-- Jahr der Veranstaltung
  |       +-- Ordner für Konzert-Assets
  +-- Ordner für alle Bilder / PDFs / etc.
```
