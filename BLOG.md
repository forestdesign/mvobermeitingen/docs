# Neue Blogeinträge schreiben

## Datenschema

### Blog-Kategorien

**JSON:**
```json
{
"name": <NAME VON KATEGORIE>,
}
```

**Bestehende Kategorien:**

- Allgemein - Angeben als: `allgemein`
- In Konzert - Angeben als: `in_konzert`
- Fasching - Angeben als: `fasching`
- Kirchenkonzert - Angeben als: `kirchenkonzert`
- Vatertag - Angeben als: `vatertag`
- Serenade - Angeben als: `serenade`
- Osterkonzert - Angeben als: `osterkonzert`

### Blogbeiträge

**Markdown:**
```md
---
title: <TITEL>                      <!-- (Freitext; Bei Sonderzeichen wie : muss der Gesamte Text in Anführungszeichen gewrappt werden "X: ...") -->  
description: <BESCHREIBUNG>         <!-- (Freitext; Bei Sonderzeichen wie : muss der Gesamte Text in Anführungszeichen gewrappt werden "X: ...") -->    
category_ref: <KATEGORIE>           <!-- (Name der Kategorie, wie oben definiert) -->
pubDate: <DATUM>                    <!-- (Format: YYYY-MM-DDTHH:MM:SS+02:00) -->

<!-- Optionale Einträge -->
heroImage: <BILD>                   <!-- (Relativer Pfad ab public\) -->
related_konzert_ref: <KONZERT>      <!-- (Relativer Pfad ab src\content\konzerte) -->
updatedDate: <DATUM>
---

<RESTLICHER INHALT GEMÄSS MARKDOWN (H1 schon von Titel vergeben)>
```

## Blogbeiträge erstellen

Um einen neuen Blogeintrag zu erstellen, muss eine neue Markdown-Datei im Ordner `src\content\blog\<JAHR>\<TITELNAME>.md` erstellt werden. Der Name der Datei wird später in der URL gezeigt und sollte daher nicht zu lange werden.

Die Datei muss mit dem oben beschriebenen Datenschema gefüllt werden. Die Datei kann mit einem beliebigen Texteditor erstellt werden. 

### Bilder
Bilder sollten immer unter `public\neuigkeiten\<JAHR>\<TITELNAME>` abgelegt werden. Der Pfad zu den Bildern wird relativ zu `public\` angegeben. Das HeroImage wird auch davon bezogen.
