# Musiker und Vorstände

## Datenschema

### Instrumenten-Gruppe

**JSON:**
  ```json
  {
  "name": <NAME VON GRUPPE>,     <!-- (Freitext) -->
  "order": <REIHENFOLGE>        <!-- (Ganzzahl; 0 = Erste Gruppe) -->
  }
  ```

### Instrument

**JSON:**
  ```json
  {
  "name": <NAME VON INSTRUMENT>,     <!-- (Freitext) -->
  "gruppe_ref": <NAME VON GRUPPE>,   <!-- (Freitext) -->
  }
  ```

### Musiker

**JSON:**
  ```json
  {
  "vorname": <NAME VON MUSIKER>,     <!-- (Freitext) -->
  "nachname": <NAME VON MUSIKER>,     <!-- (Freitext) -->
  "instrument_ref": [<NAME VON INSTRUMENT>,...],   <!-- (Name von Instrument als Array [querfloete, floete,...]) -->
  "image": <NAME VON BILD>   <!-- (Relativer Pfad von public\. In der Regel sind die Bilde runter public\musiker oder public\other ) -->
  }
  ```


### Vorstand

**JSON:**
  ```json
  {
  "rolle": <NAME der Rolle>,     <!-- (Freitext) -->
  "order": <REIHENFOLGE>,        <!-- (Ganzzahl; 0 = Erste Gruppe) -->
  "musiker_ref": <NAME VON MUSIKER-Datei>   <!-- (Name von Musiker) -->
  }
  ```

## Erstellen / Pflegen

Die Daten werden in JSON-Dateien gepflegt. Diese liegen unter `src/content/musiker/` und `src/content/vorstand/`. Die Dateien sind nach dem Schema `nachname_vorname.json` benannt. Die Dateien werden automatisch in die Webseite eingebunden.

