# Änderungen machen
1. Um Änderungen an der Webseite muss via Gitlab ein Change erzeugt werden. Dazu geht man in den Web-IDE.
  ![](assets/gitlab/gitlab_webide.png)


2. Bearbeite und erzeuge die Dateien wie gewohnt. Die Änderungen werden dadurch ermittelt. Bilder können per Drag-n-Drop erzeugt werden. Dateien über das Anlage-Symbol.

  ![](assets/gitlab/gitlab_webide2.png)

Die Webseite ist wie folgt Strukturiert:

| Ordner                          | Beschreibung                                                                                      |
|:--------------------------------|:--------------------------------------------------------------------------------------------------|
| `public`                        | Hier liegen alle Bilder, PDFs, etc.                                                               |
| `src/content`                   | Hier liegen alle dynamischen Inhalte der Webseite                                                 |
| `src/content/blog`              | Hier liegen alle Blogposts                                                                        |
| `src/content/blog_category`     | Hier liegen alle Blog-Kategorien                                                                  |
| `src/content/instrument`        | Hier liegen alle Instrumente                                                                      |
| `src/content/instrument_gruppe` | Hier liegen alle Instrument-Gruppen für die Musiker-Seite                                         |
| `src/content/konzerte`          | Hier liegen alle Konzerte                                                                         |
| `src/content/konzert_gruppe`    | Hier liegen alle Konzert-Gruppen für die Konzert-Seite                                            |
| `src/content/musiker`           | Hier liegen alle Musiker                                                                          |
| `src/content/vorstand`          | Hier liegen alle Vorstände, welche auf Musiker Referenzieren                                      |
| `src/layouts`                   | Hier liegen alle Layout-arten für die Webseite                                                    |
| `src/components`                | Hier liegen alle Componenten für die Webseite, welche für Wiederverwendungen zentral erzeugt sind |
| `src/pages`                     | Hier liegen alle Seiten, dabei wird von Hier auch die Webseiten-Struktur erzeugt.                 |
| `src/pattern`                   | Hier liegen alle komplexen wiederverwendbaren Componenten                                         |
| `src/styles`                    | Hier liegen Überschreibungsregeln für Styles, wie z.B. für Markdown.                              |



3. Änderungen commiten und pushen. Die Änderungen werden automatisch deployed. Die Webseite ist unter [https://mv-obermeitingen.de](https://mv-obermeitingen.de) erreichbar.
   Gehen Sie dazu im Web-IDE links in den Icons auf das Git-Icon und wählen Sie dort die Dateien aus, welche Sie ändern möchten.
   ![](assets/gitlab/gitlab_webide_commit.png)

4. Die Pipelines werden automatisch durchlaufen.
  ![](assets/gitlab/gitlab_pipeline.png)  

5. Es wird eine Mail an die Admins geschickt, welche über die Änderungen informiert werden.
